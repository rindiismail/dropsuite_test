# README #

The application is to count on how many files inside a path with the SAME content. It could have the same name or not

### What is this repository for? ###

* This is repository is intended only for Dropsuite test
* Version 0.1

### How do I get set up? ###

* You need php 7 in your path, never tested on php 5 but should work as well 
* You also need Sqlite PDO and Multibyte String Support enabled in your php CLI
* [Enabling Sqlite PDO in Ubuntu](https://stackoverflow.com/questions/4608863/setting-up-sqlite3-pdo-drivers-in-php)
* [Enabling Multibyte String Support in Ubuntu](https://askubuntu.com/questions/772397/mbstring-is-missing-for-phpmyadmin-in-ubuntu-16-04)


### How to run? ###

* `php search.php {path}`
* Without {path} it will scan DropsuiteTest directory which included in this repo
* So executing `php search.php` is equal with `php search.php DropsuiteTest` 