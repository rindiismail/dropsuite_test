#!/usr/bin/env php

<?php
require 'rb.php';

R::setup('sqlite:temp.db'); //This will create tmp.db file

if (isset($argv[1])) {
    $path = $argv[1];
} else {
    $path = "DropsuiteTest";
}

$dirs = scandir($path);

if (!$dirs) {
    print "Directory is not valid and sample directory doesn't exist";
} else {

    $duplicateFinder = new DuplicateFinder();
    $duplicateFinder->findDuplicate($dirs, $path);
    $result = $duplicateFinder->getMostDuplicated();

    
    if ($result) {
        print "\n\nSame content:\n\n";
        print "{$result}\n";
    } else {
        print "no file with same content\n";
    }
}
R::close();

/**
 * Find and count how many files inside a path with the same content.
 * It could have the same name or not
 */
class DuplicateFinder {

    /** 
     * Size in bytes of file chunk
     */
    protected $chunkSize = 1024*1024;

    /**
     * Find all files in given $dir
     * 
     * @param $dir array of directory name which will be searched for a file
     *        $currentPath should not be used externally
     * 
     * @return void
     */
    public function collectFiles(array $dirs, $currentPath='') 
    {
        foreach ($dirs as $k=>$dir) {
            if ($dir=='.' || $dir=='..' || $dir=='.DS_Store') {
                continue;
            }
            $dir = $currentPath.'/'.$dir;
            
            if (is_dir($dir)) {
                $subdirs = scandir($dir);

                $this->collectFiles($subdirs, $dir);
            } else {
                $file = R::dispense( 'files' );
                $file->path = $dir;
                $file->duplicate_group_id = 0; //0 = Unchecked
                R::store($file);
            }
        }
    }

    /**
     * Find duplicate file which collected by _collectFiles_ method
     * 
     * @param 
     * 
     * @return void
     */
    public function findDuplicate(array $dirs, $currentPath='') 
    {
        R::nuke();

        $this->collectFiles($dirs, $currentPath);

        $fileCount = R::count('files');

        if ($fileCount==0) {
            return;
        }

        $fileIterator1 = R::findCollection('files', ' ORDER BY id ASC');
        $file1 = $fileIterator1->next();
        
        while($file1) {
            print "check {$file1->path}\n";
            
            if ($file1->duplicate_group_id!=0) { //skip if already known as duplicate, improve overal performance
                print "skip\n";
                $file1 = $fileIterator1->next();
                continue;
            }

            $fileIterator2 = R::findCollection('files', "id>{$file1->id} AND duplicate_group_id=0 ORDER BY id ASC");
            $file2 = $fileIterator2->next();

            while($file2) {
                print "   againts {$file2->path}\n";

                if ($this->isEqual($file1->path, $file2->path)) {
                    if ($file1->duplicate_group_id==0) {
                        $last_duplicate_group_id = R::getCell('SELECT MAX(duplicate_group_id) FROM files WHERE duplicate_group_id<>0'); 
                        
                        $file1->duplicate_group_id = $last_duplicate_group_id + 1;
                        R::store($file1);
                    }

                    $file2->duplicate_group_id = $file1->duplicate_group_id;
                    R::store($file2);
                }

                $file2 = $fileIterator2->next();
            }
            $fileIterator2->close();

            $file1 = $fileIterator1->next();
        }

        $fileIterator1->close();
    }


    /**
     * compare two file using file stream
     * 
     * @param $file1, $file2
     * 
     * @return true if $file1 and $file2 identical, false otherwise
     */
    public function isEqual($file1, $file2) {
        $buffer1 = '';
        $buffer2 = '';
        $handle1 = fopen($file1, 'rb');
        $handle2 = fopen($file2, 'rb');


        if ($handle1 === false || $handle2 === false) {
            return false;
        }
    
        while (!feof($handle1) && !feof($handle2)) {
            $buffer1 = fread($handle1, $this->chunkSize);
            $buffer2 = fread($handle2, $this->chunkSize);

            if ($buffer1 !== $buffer2) {
                return false;
            }
        }

        if (feof($handle1) != feof($handle2)) {
            return false;
        }
    
        fclose($handle1); 
        fclose($handle2);
    
        return true;
    }

    /**
     * get content with most duplicated
     * 
     * @return void
     */
    public function getMostDuplicated()
    {
        $most_duplicated = R::getRow("SELECT duplicate_group_id, COUNT(duplicate_group_id) as count FROM files GROUP BY duplicate_group_id HAVING COUNT(duplicate_group_id) = (SELECT MAX(count) FROM (SELECT COUNT(duplicate_group_id) AS count FROM files GROUP BY duplicate_group_id))");
        if($most_duplicated) {
            $file = R::findOne('files', "duplicate_group_id={$most_duplicated['duplicate_group_id']}");
            $handle = fopen($file->path, 'rb');
            $preview = fread($handle, 1024);
            if (!feof($handle)) {
                $preview .= '...';
            }
            
            $preview .= '    '.($most_duplicated['count']);
            fclose($handle);
            return $preview;
        } else {
            return null;
        }  
    }
}

?>